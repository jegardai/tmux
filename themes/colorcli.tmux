#===============================================================================
#
# From : https://github.com/jonasjacek/colorcli
# Changes:
# * Update status-right
# * Invert forground (color24) and background (black/grey) to increase difference
#   with local tmux.
# * Add some flags (window_last, window_zoomed,…)
#
# DESIGN
#
# DESIGN: 256 COLOR SUPPORT
setw -g default-terminal "screen-256color"

# DESIGN: PANES
set -g pane-active-border-style fg=color24
set -g pane-border-style fg=color24

# DESIGN: MESSAGING
set -g message-command-style fg=blue,bg=black
set -g message-style fg=color59,bg=color226

# DESIGN: MODES
setw -g clock-mode-colour color135
set -g mode-style fg=color160,bold,bg=color238,bold

# DESIGN: STATUSBAR BOTTOM
set -g status-style fg=white,bg=color24
set -g status-interval 2
set -g status-position bottom

# DESIGN: WINDOW STATUS
# new: window-active-style
# new: window-status-activity-style
# new:  window-status-bell-style fg=white,bg=black
# new: window-status-current-style
# new: window-status-last-style
# new: window-status-style
# new: window-style
setw -g window-status-format " #F#I:#W#F "
setw -g window-status-current-format " #F#I:#W#F "
setw -g window-status-format "#[fg=color0,bold]#[bg=color24]#I:#W#[fg=color255]#{?window_last_flag,⎗ ,}#{?window_zoomed_flag,🔍,}"
setw -g window-status-current-format "#[fg=color255]#[bg=color0]#I:#[fg=color24,bold]#W#{?window_zoomed_flag,🔍,} "
#setw -g window-status-current-bg pink
#setw -g window-status-current-fg grey
#setw -g window-status-current-attr dim
#setw -g window-status-bg green
#setw -g window-status-fg black
#set -g status-right-length 50
set -g status-left-length 20
# items left to the tabs
set -g status-left ''
# items right to the tabs
#set -g status-right '#[fg=color59,bg=color24,bold] %y/%m/%d |#[fg=color59,bg=color24,bold] %H:%M:%S '
# #(cat /proc/loadavg | cut -d " " -f 1,2,3) : CPU load
set -g status-right '#[fg=color255,bg=color24,bold] #H #[fg=color0,bg=color24,bold][ #[fg=color232,bg=color24,bold]#(cat /proc/loadavg | cut -d " " -f 1)#[fg=color234,bg=color24,bold] #(cat /proc/loadavg | cut -d " " -f 2)#[fg=color236,bg=color24,bold] #(cat /proc/loadavg | cut -d " " -f 3) #[fg=color0,bg=color24,bold]]'
#set -g status-right ''

setw -g window-status-bell-style fg=red,bold
set-window-option -g window-status-bell-style bg=color160
