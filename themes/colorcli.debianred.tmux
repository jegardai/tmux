#===============================================================================
#
# From : https://github.com/jonasjacek/colorcli
# Changes:
# * Update status-right
# * Update color24 -> color125
# * Add some flags (window_last, window_zoomed,…)
#
# DESIGN
#
# DESIGN: 256 COLOR SUPPORT
setw -g default-terminal "screen-256color"

### colors {{{
black="#000000"
lightblack="#151515"
blue="#0094cc"
blue_tomorrow="#6699cc"
grey="#909090"
darkgrey="#26221c"
grey_tomorrow="#2d2d2d"
lightgreen="#9fbc00"
green="#345f0c"
pink="#b00a44"
lightpink="#ff0675"
orange_tomorrow="#f99157"
red="#ff0000"
darkred="#8f0000"
debianred="#e70a53"
white="#efefef"

### }}}

# DESIGN: PANES
set -g pane-active-border-style fg=color125,bg=color250
set -g pane-border-style fg=color250,bg=color250

# DESIGN: MESSAGING
set -g message-command-style fg=blue,bg=black
#set -g message-style fg=color59,bg=color226
set -g message-style fg=color59,bg=color220

# DESIGN: MODES
#setw -g clock-mode-color color135
setw -g clock-mode-colour color125
set -g mode-style fg=color160,bold,bg=color238,bold

# DESIGN: STATUSBAR BOTTOM
set -g status-style fg=white,bg=color250
set -g status-interval 2
set -g status-position bottom

# DESIGN: WINDOW STATUS
# new: window-active-style
# new: window-status-activity-style
# new:  window-status-bell-style fg=white,bg=black
# new: window-status-current-style
# new: window-status-last-style
# new: window-status-style
# new: window-style
setw -g window-status-format " #F#I:#W#F "
setw -g window-status-current-format " #F#I:#W#F "
#setw -g window-status-format "#[fg=white]#[bg=color59] #I #[fg=color59]#[bg=color254] #W "
setw -g window-status-format "#[fg=white]#[bg=color59] #I #[fg=color59]#[bg=color254] #W#[fg=color125]#{?window_last_flag,⎗ ,}#{?window_zoomed_flag,🔍,} "
#setw -g window-status-current-format "#[fg=white]#[bg=color24] #I #[fg=color24]#[bg=color254] #W "
setw -g window-status-current-format "#[fg=color255]#[bg=color125] #I #[fg=color125]#[bg=color254] #W#{?window_zoomed_flag,🔍,} "
#setw -g window-status-current-bg pink
#setw -g window-status-current-fg grey
#setw -g window-status-current-attr dim
#setw -g window-status-bg green
#setw -g window-status-fg black
set -g status-left-length 20
# items left to the tabs
set -g status-left ''
# items right to the tabs
set -g status-right '#[fg=color125,bg=color250,bold] #H #[fg=color59,bg=color250,bold]| %H:%M |#[fg=color59,bg=color250,bold] %y-%m-%d '
if '[ -z "$DISPLAY" ]' 'set -g status-right "#H #(acpi | cut -d \" \" -f4) [#[fg=color125,bright]%H:%M:%S#[default]]"'
#set -g status-right ''

setw -g window-status-bell-style fg=red,bold
set-window-option -g window-status-bell-style bg=color160
