
### colors {{{
black="#000000"
lightblack="#151515"
blue="#0094cc"
blue_tomorrow="#6699cc"
grey="#909090"
darkgrey="#26221c"
grey_tomorrow="#2d2d2d"
lightgreen="#9fbc00"
green="#345f0c"
pink="#b00a44"
lightpink="#ff0675"
orange_tomorrow="#f99157"
red="#ff0000"
darkred="#8f0000"
debianred="#e70a53"
white="#efefef"

### }}}

### statusbar {{{

# Attribut list {{{
## bright/bold:
## dim:
## underscore:
## blink: clignotant
## reverse: reverse background and foreground colors
## hidden:
## italics:
## overline: Needs Smol extension, see the manual
## strikethrough: barré
## double-underscore:
## curly-underscore:
## dotted-underscore:
## dashed-underscore:
# }}}

# Status line style
## BackGround and ForeGround
set -g status-style fg="${debianred}",bg=black

# Status line style for a single window
## BackGround, ForeGround and attribut
set -g window-status-style bg="${darkgrey}",fg="${debianred}",bold

# Status line style for the currently active window
## BackGround, ForeGround and attribut
set -g window-status-current-style bg="${debianred}",fg=black,dim

# selection
set -wg mode-style bg="${debianred}",fg=white,bold

set -g clock-mode-colour "${debianred}"

## Terminal avec notification
# Couleur du foreground
# Couleur du background
# Attribut
#setw -g window-status-alert-attr default
#setw -g window-status-alert-fg red
#setw -g window-status-alert-bg default

# Pane border style for panes aside the active one
set -g pane-border-style fg=white

# Pane border style for the currently active pane
set -g pane-active-border-style fg="${debianred}"

# right statusbar
# %H:%M:%S : Affiche de l'heure (actualisation lors de manipulation ou en fonction de la valeur de status-interval)
#set -g status-right "⚡[#[fg="${darkred}",bright]%H:%M:%S#[default]]"

# Additionnal info if no display available (aka tty)
# #H: hostname
# #(acpi | cut -d \" \" -f4): display percentage of battery
if '[ -z "$DISPLAY" ]' 'set -g status-right "#H #(acpi | cut -d \" \" -f4) [#[fg="${darkred}",bright]%H:%M:%S#[default]]"'

### }}}
